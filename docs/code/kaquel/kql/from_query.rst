``kaquel.kql.from_query`` -- Elasticsearch to KQL query conversion
==================================================================

.. automodule:: kaquel.kql.from_query
