``kaquel.kql.to_query`` -- KQL to Elasticsearch query conversion
================================================================

.. automodule:: kaquel.kql.to_query
