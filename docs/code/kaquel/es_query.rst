``kaquel.es_query`` -- ElasticSearch query DSN related utilities
================================================================

.. toctree::
    :maxdepth: 1

    es_query/lang
    es_query/parser

.. automodule:: kaquel.es_query
